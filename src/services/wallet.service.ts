import { Injectable } from '@angular/core';
import { Wallet } from '../interfaces/wallet';

@Injectable({
  providedIn: 'root',
})
export class WalletService {
  constructor() {}

  public getWallets(): Wallet[] {
    const rawWallets = localStorage.getItem('wallets');

    if (rawWallets) {
      const Wallets: Wallet[] = JSON.parse(rawWallets) ?? [];
      return Wallets;
    } else {
      return [];
    }
  }
}
