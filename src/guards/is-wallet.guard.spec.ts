import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { isWalletGuard } from './is-wallet.guard';

describe('isWalletGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => isWalletGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
