export interface Wallet {
    address: string,
    private: string,
    public: string
}
