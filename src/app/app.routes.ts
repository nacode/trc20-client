import { Routes } from '@angular/router';
import { WalletCreatorComponent } from '../core/wallet-creator/wallet-creator.component';
import { WalletManagerComponent } from '../core/wallet-manager/wallet-manager.component';
import { isWalletGuard } from '../guards/is-wallet.guard';
import { WalletSelectorComponent } from '../core/wallet-manager/components/wallet-selector/wallet-selector.component';
import { WalletSingleComponent } from '../core/wallet-single/wallet-single.component';

export const routes: Routes = [
  // {
  //   path: 'creator',
  //   component: WalletCreatorComponent,
  // },

  {
    path: 'manager',
    component: WalletManagerComponent,
    canActivate: [isWalletGuard],
    children: [      
      {
        path: '',
        component: WalletSelectorComponent,
      },
      {
        path: ':id',
        component: WalletSingleComponent,
      },
      
    ],
  },

  // {
  //   path: '',
  //   redirectTo: 'manager',
  //   pathMatch: 'full',
  // },
];
