import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletCreatorComponent } from './wallet-creator.component';

describe('WalletCreatorComponent', () => {
  let component: WalletCreatorComponent;
  let fixture: ComponentFixture<WalletCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WalletCreatorComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(WalletCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
