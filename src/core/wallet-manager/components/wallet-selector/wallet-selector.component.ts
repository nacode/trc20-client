import { Component } from '@angular/core';
import { PageHeaderComponent } from '../../../page-header/page-header.component';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { CommonModule } from '@angular/common';
import { Wallet } from '../../../../interfaces/wallet';
import { WalletService } from '../../../../services/wallet.service';

@Component({
  selector: 'app-wallet-selector',
  standalone: true,
  imports: [NzEmptyModule, CommonModule],
  templateUrl: './wallet-selector.component.html',
  styleUrl: './wallet-selector.component.scss',
})
export class WalletSelectorComponent {
  public wallets: Wallet[] = [] as Wallet[];

  constructor(private walletService: WalletService) {
    const walletsStore: Wallet[] = this.walletService.getWallets();
    this.wallets = walletsStore;
  }
}
