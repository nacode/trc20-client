import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { WalletSelectorComponent } from './components/wallet-selector/wallet-selector.component';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { PageHeaderComponent } from '../page-header/page-header.component';
import { WalletService } from '../../services/wallet.service';
import { Wallet } from '../../interfaces/wallet';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

@Component({
  selector: 'app-wallet-manager',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    WalletSelectorComponent,
    PageHeaderComponent,
  ],
  templateUrl: './wallet-manager.component.html',
  styleUrl: './wallet-manager.component.scss',
  providers: [],
})
export class WalletManagerComponent {}
