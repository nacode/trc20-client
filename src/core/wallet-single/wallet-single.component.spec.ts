import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletSingleComponent } from './wallet-single.component';

describe('WalletSingleComponent', () => {
  let component: WalletSingleComponent;
  let fixture: ComponentFixture<WalletSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WalletSingleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(WalletSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
